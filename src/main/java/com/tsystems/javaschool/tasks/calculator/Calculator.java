package com.tsystems.javaschool.tasks.calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {


    private String binaryOperation(String first, char operation, String last) {
        switch (operation) {
            case '+':
                return Double.toString(new Double(first) + new Double(last));

            case '-':

                return Double.toString(new Double(first) - new Double(last));

            case '*':

                return Double.toString(new Double(first) * new Double(last));
            case '/': {

                double lastDouble = new Double(last);
                if (lastDouble == 0) {
                    return null;
                }

                return Double.toString(new Double(first) / lastDouble);
            }
        }
        return null;
    }

    private String matcherOperations(String patt, String expression, String endCondition) {
        do {
            Pattern pattern = Pattern.compile(patt);
            Matcher matcher = pattern.matcher(expression);
            while (matcher.find()) {
                String res = binaryOperation(matcher.group(1), matcher.group(2).charAt(0), matcher.group(3));
                if (res == null) {
                    return null;
                }
                expression = matcher.replaceFirst(res);
                matcher = pattern.matcher(expression);
            }
        }
        while (Pattern.compile(endCondition).matcher(expression).find());
        return expression;
    }


    private String calculateExpression(String expression) {

        do {
            Pattern pattern = Pattern.compile("(\\(([^()]*)\\))");
            Matcher matcher = pattern.matcher(expression);
            while (matcher.find()) {
                String res = calculateExpression(matcher.group(2));
                if (res == null) {
                    return null;
                }
                expression = matcher.replaceFirst(res);
                matcher = pattern.matcher(expression);
            }
        }
        while (Pattern.compile("[()]").matcher(expression).find());


        expression = matcherOperations("((?:^\\-)?\\d+(?:\\.\\d+)?)([\\*\\/])((?:\\-)?\\d+(?:\\.\\d+)?)", expression, "[\\*\\/]");

        if (expression == null) {
            return null;
        }

        expression = matcherOperations("((?:^\\-)?\\d+(?:\\.\\d+)?)([\\+\\-])((?:\\-)?\\d+(?:\\.\\d+)?)", expression, "[^$\\-][\\+\\-]");

        if (expression == null) {
            return null;
        }

        return expression;
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    String evaluate(String statement) {
        if ((statement == null) || (statement.equals(""))) {
            return null;
        }
        String result = statement;
        result = result.replaceAll(" ", "");
        if (Pattern.compile("(^[^)(]+\\))|(?:[^0-9+\\-*/.()]+)|(?:[+\\-*/.]{2})|(?:[+\\-*/.]$)|([0-9]\\()|(\\)[0-9])").matcher(result).find()) {
            return null;
        }

        if (result.replaceAll("[^(]", "").length() != result.replaceAll("[^)]", "").length()) {
            return null;
        }

        result = calculateExpression(result);
        if (result == null) {
            return null;
        }
        return Double.toString((double) (Math.round(new Double(result) * 10000)) / 10000).replaceFirst("\\.[0]+", "");
    }


}
