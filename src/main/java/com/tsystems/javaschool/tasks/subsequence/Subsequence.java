package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if ((x == null) || (y == null)) {
            throw new IllegalArgumentException();
        }

        if (x.size() == 0) {
            return true;
        }

        if (y.size() == 0) {
            return false;
        }

        ArrayList<Integer>[] itemsEntries = new ArrayList[x.size()];

        int masSize = 1;

        for (int i = 0; i < x.size(); ++i) {
            itemsEntries[i] = new ArrayList<>();
            boolean elemExist = false;
            for (int j = 0; j < y.size(); ++j) {

                if (y.get(j).equals(x.get(i))) {
                    itemsEntries[i].add(j);
                    elemExist = true;
                }
            }
            if (!elemExist) {
                return false;
            }
            masSize *= itemsEntries[i].size();
        }

        int[][] mas = new int[masSize][x.size()];

        boolean result;

        for (int i = 0; i < masSize; ++i) {
            for (int j = 0; j < x.size(); ++j) {
                mas[i][j] = itemsEntries[j].get(i % itemsEntries[j].size());
            }
            result = true;
            for (int j = 0; j < x.size() - 1; ++j) {
                if (mas[i][j] > mas[i][j + 1]) {
                    result = false;
                    break;
                }
            }
            if (result) {
                return true;
            }
        }

        return false;

    }
}
