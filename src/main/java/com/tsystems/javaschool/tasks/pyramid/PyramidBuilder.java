package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;


public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        double x = (Math.sqrt(1 + 8 * inputNumbers.size()) - 1) / 2;

        if ((x % 1) != 0) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        int count = (int) x;

        int index = 0;
        int[][] result = new int[count][count * 2 - 1];


        for (int i = 0; i < count; ++i) {
            for (int j = count - i - 1; j <= (count - i - 1) + i * 2; j += 2) {
                result[i][j] = inputNumbers.get(index++);
            }

        }
        return result;
    }


}
